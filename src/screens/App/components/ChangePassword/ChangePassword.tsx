import React from 'react';
import {StyleSheet} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Box, Text, theme} from '../../../../components';
import {AppRoutes, StackNavigationProps} from '../../../../navigation';
import {Feather} from '@expo/vector-icons';
import {HeaderBar} from '../../../../components';
import {ChangePasswordForm} from './components';

type ChangePasswordNavigation = StackNavigationProps<
  AppRoutes,
  'ChangePassword'
>;
interface ChangePasswordProps extends ChangePasswordNavigation {}

const ChangePassword: React.FC<ChangePasswordProps> = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <HeaderBar
        label="Mật khẩu"
        backIcon={<Feather name="x" size={32} color={theme.colors.alto} />}
        onBackPress={() => navigation.navigate('Home')}
      />
      <Box padding="m">
        <ChangePasswordForm />
      </Box>
    </SafeAreaView>
  );
};

export default ChangePassword;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});
