import React from 'react';
import {Formik, FormikHelpers} from 'formik';
import {Alert} from 'react-native';
import * as yup from 'yup';
import {Box, Text, TextInput, Button} from '../../../../../components';
import {useDispatch, useSelector} from 'react-redux';
import {userRegister} from '../../../../../redux/actions/authentication';
import IStore from '../../../../../redux/model/store/IStore';

const validationLoginSchema = yup.object().shape({
  email: yup
    .string()
    .email('Email không hợp lệ')
    .required('Email là trường bắt buộc'),
  password: yup.string().required('Mật khẩu là trường bắt buộc'),
  rePassword: yup.string().required('Mật khẩu là trường bắt buộc'),
});

interface FormRegisterValues {
  email: string;
  password: string;
  rePassword: string;
}

const RegisterForm = () => {
  const dispatch = useDispatch();
  const AuthState = useSelector((state: IStore) => state.Auth);
  const {isProcessing} = AuthState;

  const initialValues = {
    email: '',
    password: '',
    rePassword: '',
  };

  const onSubmit = (
    values: FormRegisterValues,
    {setSubmitting}: FormikHelpers<FormRegisterValues>,
  ) => {
    dispatch(userRegister({email: values.email, password: values.password}));
  };

  const showError = (errors: string, setErrors: () => void) => {
    Alert.alert('Thông báo', errors, [
      {
        text: 'Ok',
        onPress: setErrors,
      },
    ]);
  };

  return (
    <React.Fragment>
      <Formik
        validateOnBlur={false}
        validateOnChange={true}
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationLoginSchema}>
        {({handleChange, handleSubmit, errors, values, isValid}) => (
          <>
            <Box marginBottom="l">
              <TextInput
                value={values.email}
                placeholder="Email"
                onChangeText={handleChange('email')}
              />
              <TextInput
                value={values.password}
                placeholder="Mật khẩu"
                onChangeText={handleChange('password')}
                secureTextEntry={true}
              />
              <TextInput
                value={values.rePassword}
                placeholder="Xác nhận mật khẩu"
                onChangeText={handleChange('rePassword')}
                secureTextEntry={true}
              />
              {!isValid && (
                <Box marginBottom="m">
                  <Text
                    variant="body"
                    color="persianRed"
                    fontSize={15}
                    letterSpacing={2}>
                    {errors[Object.keys(errors)[0]]}
                  </Text>
                </Box>
              )}

              <Button
                loading={isProcessing}
                variant="secondary"
                label="Đăng Ký"
                onPress={handleSubmit}
                disabled={!isValid}
              />
            </Box>
          </>
        )}
      </Formik>
    </React.Fragment>
  );
};

export default RegisterForm;
