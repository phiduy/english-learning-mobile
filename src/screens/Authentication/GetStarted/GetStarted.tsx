import React from 'react';
import {StyleSheet} from 'react-native';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Box, Text, Button, theme} from '../../../components';
import {Feather} from '@expo/vector-icons';
import {AuthenticationRoutes, StackNavigationProps} from 'navigation';

type GetStartedNavigation = StackNavigationProps<
	AuthenticationRoutes,
	'GetStarted'
>;

export interface GetStartedProps extends GetStartedNavigation {}

const GetStated: React.FC<GetStartedProps> = ({navigation}) => {
	return (
		<SafeAreaView style={styles.container}>
			<Box flex={1} padding="m" position="relative">
				<Box flex={1} justifyContent="flex-end">
					<Box marginBottom="xl" alignItems="center">
						<Text variant="title" marginBottom="m">
							Đã có tài khoản?
						</Text>
						<Text variant="subTitle">
							Tiếp tực từ phần bạn đang bỏ dở
						</Text>
					</Box>
					<Button
						variant="primary"
						label="Đăng nhập"
						onPress={() => navigation.navigate('Login')}
					/>
				</Box>
				<Box
					height={2}
					backgroundColor="borderColorDefault"
					marginBottom="xl"
					marginTop="xl"
				/>
				<Box flex={1}>
					<Box marginBottom="xl" alignItems="center">
						<Text variant="title" marginBottom="m">
							Bạn mới tham gia CatEng?
						</Text>
						<Text variant="subTitle">
							Bắt đầu học tiếng anh ngay
						</Text>
					</Box>
					<Button
						variant="default"
						label="Bắt đầu ngay"
						onPress={() => navigation.navigate('Register')}
					/>
				</Box>
			</Box>
		</SafeAreaView>
	);
};

export default GetStated;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'white',
	},
});
