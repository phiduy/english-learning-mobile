import {AuthenticationRoutes, StackNavigationProps} from '../../../navigation';
import React from 'react';
import {Dimensions, StyleSheet, View} from 'react-native';
import Animated, {divide, multiply} from 'react-native-reanimated';
import {
  interpolateColor,
  useScrollHandler,
} from 'react-native-redash/lib/module/v1';
import {Dot} from '../../../components';
import {Slide, SubSlide, SLIDE_HEIGHT} from './components';

const {width} = Dimensions.get('window');
const BORDER_RADIUS = 75;

const slides = [
  {
    title: 'Upgrade',
    subtitle: 'Improve Your Skill',
    description:
      ' Create your individual & improve your skill everyday',
    color: '#BFEAF5',
  },
  {
    title: 'Short',
    subtitle: 'For beginner',
    description: ' This is especially important for beginner English learner',
    color: '#BEECC4',
  },
  {
    title: 'Funny',
    subtitle: 'No stress about score',
    description: ' The more you comfortable, the easier to learn new skill',
    color: '#FFE4D9',
  },
  {
    title: 'Sound',
    subtitle: 'Look Good, Sound Good',
    description: ' Sound make you easier to learn',
    color: '#FFDDDD',
  },
];

type OnBoardingNavigation = StackNavigationProps<
  AuthenticationRoutes,
  'OnBoarding'
>;
export interface OnBoardingProps extends OnBoardingNavigation {}

const OnBoarding: React.FC<OnBoardingProps> = ({navigation}) => {
  const scroll = React.useRef<Animated.ScrollView>(null);
  const {x, scrollHandler} = useScrollHandler();
  const backgroundColor = interpolateColor(x, {
    inputRange: slides.map((_, i) => i * width),
    outputRange: slides.map((slide) => slide.color),
  });

  return (
    <View style={styles.container}>
      <Animated.View style={[styles.slider, {backgroundColor}]}>
        <Animated.ScrollView
          ref={scroll}
          horizontal
          snapToInterval={width}
          decelerationRate="fast"
          showsHorizontalScrollIndicator={false}
          bounces={false}
          {...scrollHandler}>
          {slides.map(({title}, index) => (
            <Slide key={index} right={!!(index % 2)} {...{title}} />
          ))}
        </Animated.ScrollView>
      </Animated.View>
      <View style={styles.footer}>
        <Animated.View
          style={{
            ...StyleSheet.absoluteFillObject,
            backgroundColor,
          }}
        />

        <Animated.View style={styles.footerContent}>
          <View style={styles.pagination}>
            {slides.map((_, index) => (
              <Dot key={index} currentIndex={divide(x, width)} {...{index}} />
            ))}
          </View>
          <Animated.View
            style={{
              flex: 1,
              flexDirection: 'row',
              width: width * slides.length,
              transform: [
                {
                  translateX: multiply(x, -1),
                },
              ],
            }}>
            {slides.map(({subtitle, description}, index) => (
              <SubSlide
                key={index}
                onPress={() => {
                  if (scroll.current) {
                    scroll.current.getNode().scrollTo({
                      x: width * (index + 1),
                      animated: true,
                    });
                    if (index === slides.length - 1) {
                      navigation.navigate('GetStarted');
                    }
                  }
                }}
                last={index === slides.length - 1}
                {...{subtitle, description}}
              />
            ))}
          </Animated.View>
        </Animated.View>
      </View>
    </View>
  );
};

export default OnBoarding;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  pagination: {
    ...StyleSheet.absoluteFillObject,
    height: BORDER_RADIUS,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  slider: {
    height: SLIDE_HEIGHT,
    borderBottomRightRadius: BORDER_RADIUS,
  },
  footer: {
    flex: 1,
  },
  footerContent: {
    flex: 1,
    backgroundColor: 'white',
    borderTopLeftRadius: BORDER_RADIUS,
  },
});
