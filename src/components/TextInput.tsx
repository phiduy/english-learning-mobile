import React from 'react';
import {
  TextInput as RnTextInput,
  StyleSheet,
  TextInputProps,
  TouchableWithoutFeedback,
} from 'react-native';
import theme, {Box, Text} from './Theme';
import {Feather} from '@expo/vector-icons';

interface CusTextInputProps extends TextInputProps {
  label?: string;
  secureTextEntry?: boolean;
  suffix?: React.ReactNode;
}

const TextInput = ({
  label,
  suffix,
  secureTextEntry = false,
  ...reset
}: CusTextInputProps) => {
  const inputElementRef = React.useRef<RnTextInput>(null);
  const [visibleSecureEntry, setVisibleSecureEntry] = React.useState(
    secureTextEntry,
  );

  React.useEffect(() => {
    inputElementRef.current?.setNativeProps({
      style: {
        fontFamily: 'DinRoundPro-Medi',
      },
    });
  }, []);

  return (
    <Box marginBottom="m" position="relative">
      {label && (
        <Text variant="label" marginBottom="s">
          {label}
        </Text>
      )}
      <RnTextInput
        {...reset}
        secureTextEntry={visibleSecureEntry}
        ref={inputElementRef}
        style={styles.container}
      />
      {secureTextEntry || suffix ? (
        <Box
          position="absolute"
          right={0}
          top={0}
          paddingRight="m"
          paddingBottom="m"
          justifyContent="flex-end"
          height="100%">
          {secureTextEntry ? (
            <TouchableWithoutFeedback
              onPress={() => setVisibleSecureEntry(!visibleSecureEntry)}>
              <Feather
                name={visibleSecureEntry ? 'eye-off' : 'eye'}
                size={24}
                color={theme.colors.muted}
              />
            </TouchableWithoutFeedback>
          ) : (
            suffix
          )}
        </Box>
      ) : null}
    </Box>
  );
};

const styles = StyleSheet.create({
  container: {
    borderColor: theme.colors.borderColorDefault,
    borderWidth: 2,
    borderRadius: theme.borderRadius.m,
    fontSize: 16,
    letterSpacing: 1,
    paddingHorizontal: theme.spacing.m,
    paddingVertical: 12,
    color: theme.colors.text,
    fontFamily: 'DinRoundPro-Medi',
  },
});

export default TextInput;
