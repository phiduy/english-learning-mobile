import React from 'react';
import {TouchableWithoutFeedback} from 'react-native';
import theme, {Box, Text} from './Theme';
import {Feather} from '@expo/vector-icons';

interface HeaderBarProps {
  onBackPress?: () => void;
  showBackButton?: boolean;
  border?: boolean;
  backIcon?: React.ReactNode;
  label?: string;
  rightButton?: React.ReactNode;
}

const HeaderBar = ({
  onBackPress,
  showBackButton = true,
  border = true,
  rightButton,
  backIcon,
  label,
}: HeaderBarProps) => {
  return (
    <Box
      position="relative"
      flexDirection="row"
      justifyContent="center"
      backgroundColor="white"
      borderBottomWidth={border ? 1 : 0}
      paddingTop="s"
      paddingBottom="m"
      borderBottomColor="borderColorDefault">
      {label && <Text variant="title2">{label}</Text>}

      {showBackButton && (
        <Box
          position="absolute"
          top={0}
          left={0}
          height="100%"
          paddingTop="s"
          paddingLeft="s">
          <TouchableWithoutFeedback onPress={onBackPress}>
            {backIcon ? (
              backIcon
            ) : (
              <Feather name="arrow-left" size={32} color={theme.colors.alto} />
            )}
          </TouchableWithoutFeedback>
        </Box>
      )}
      {rightButton && (
        <Box
          position="absolute"
          top={0}
          right={0}
          height="100%"
          paddingTop="s"
          paddingRight="s">
          {rightButton}
        </Box>
      )}
    </Box>
  );
};

export default HeaderBar;
