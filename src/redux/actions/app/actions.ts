import {
  ILessonRecord,
  IPagination,
  IVocabularyPackage,
} from './../../model/IApp';
import Keys from './actionKeys';
import * as IActions from './IActions';
import {ILesson, IUserInfo} from '../../model/IApp';
import { ImagePickerResult } from 'expo-image-picker';

export const handleClear = ({
  type,
}: {
  type: 'update' | 'postUserHistory';
}): IActions.IHandleClear => {
  return {
    type: Keys.HANDLE_CLEAR,
    payload: {
      type,
    },
  };
};

export const handleUserAnswer = (data: any): IActions.IHandleUserAnswer => {
  return {
    type: Keys.HANDLE_USER_ANSWER,
    payload: data,
  };
};

export const handleCurrentLesson = (data: {
  type: 'detail';
  lesson: ILesson;
}): IActions.IHandleCurrentLesson => {
  return {
    type: Keys.HANDLE_CURRENT_LESSON,
    payload: data,
  };
};

export const userLogOut = (): IActions.IUserLogOut => {
  return {
    type: Keys.USER_LOG_OUT,
  };
};

//#region Get Lessons Actions
export const getLessons = (data: {
  page?: number;
  limit?: number;
  maxLevel: number;
}): IActions.IGetLessons => {
  return {
    type: Keys.GET_LESSONS,
    payload: data,
  };
};

export const getLessonsSuccess = (
  res: ILessonRecord,
): IActions.IGetLessonsSuccess => {
  return {
    type: Keys.GET_LESSONS_SUCCESS,
    payload: res,
  };
};

export const getLessonsFail = (res: any): IActions.IGetLessonsFail => {
  return {
    type: Keys.GET_LESSONS_FAIL,
    payload: {
      errors: res,
    },
  };
};
//#endregion

//#region Get Lesson By Id Actions
export const getLessonById = (data: {_id: string}): IActions.IGetLessonById => {
  return {
    type: Keys.GET_LESSON_BY_ID,
    payload: data,
  };
};

export const getLessonByIdSuccess = (
  res: any[],
): IActions.IGetLessonByIdSuccess => {
  return {
    type: Keys.GET_LESSON_BY_ID_SUCCESS,
    payload: res,
  };
};

export const getLessonByIdFail = (res: any): IActions.IGetLessonByIdFail => {
  return {
    type: Keys.GET_LESSON_BY_ID_FAIL,
    payload: {
      errors: res,
    },
  };
};
//#endregion

//#region Get Vocabulary Packages Actions
export const getVocabularyPackages = (data: {
  root?: boolean;
  parent?: string;
}): IActions.IGetVocabularyPackages => {
  return {
    type: Keys.GET_VOCABULARY_PACKAGES,
    payload: data,
  };
};

export const getVocabularyPackagesSuccess = (
  res: any[],
): IActions.IGetVocabularyPackagesSuccess => {
  return {
    type: Keys.GET_VOCABULARY_PACKAGES_SUCCESS,
    payload: res,
  };
};

export const getVocabularyPackagesFail = (
  res: any,
): IActions.IGetVocabularyPackagesFail => {
  return {
    type: Keys.GET_VOCABULARY_PACKAGES_FAIL,
    payload: {
      errors: res,
    },
  };
};
//#endregion

//#region Get Vocabulary Actions
export const getVocabulary = (data: {_id: string}): IActions.IGetVocabulary => {
  return {
    type: Keys.GET_VOCABULARY,
    payload: data,
  };
};

export const getVocabularySuccess = (res: {
  data: IVocabularyPackage[];
  pagination: IPagination;
}): IActions.IGetVocabularySuccess => {
  return {
    type: Keys.GET_VOCABULARY_SUCCESS,
    payload: res,
  };
};

export const getVocabularyFail = (res: any): IActions.IGetVocabularyFail => {
  return {
    type: Keys.GET_VOCABULARY_FAIL,
    payload: {
      errors: res,
    },
  };
};
//#endregion

//#region Get User History Actions
export const getUserHistory = (data: {
  day: string;
  beginDate: string;
}): IActions.IGetUserHistory => {
  return {
    type: Keys.GET_USER_HISTORY,
    payload: data,
  };
};

export const getUserHistorySuccess = (
  res: any,
): IActions.IGetUserHistorySuccess => {
  return {
    type: Keys.GET_USER_HISTORY_SUCCESS,
    payload: res,
  };
};

export const getUserHistoryFail = (res: {
  message: string;
}): IActions.IGetUserHistoryFail => {
  return {
    type: Keys.GET_USER_HISTORY_FAIL,
  };
};
//#endregion

//#region Post User History Actions
export const postUserHistory = (data: {
  isAddGamesLevel: boolean;
  isAddVocabularyLevel: boolean;
  gameId: string;
  note?: string;
}): IActions.IPostUserHistory => {
  return {
    type: Keys.POST_USER_HISTORY,
    payload: data,
  };
};

export const postUserHistorySuccess = (
  res: IUserInfo,
): IActions.IPostUserHistorySuccess => {
  return {
    type: Keys.POST_USER_HISTORY_SUCCESS,
    payload: res,
  };
};

export const postUserHistoryFail = (
  res: any,
): IActions.IPostUserHistoryFail => {
  return {
    type: Keys.POST_USER_HISTORY_FAIL,
    payload: {
      errors: res,
    },
  };
};
//#endregion

//#region Get User Info Actions
export const getUserInfo = (): IActions.IGetUserInfo => {
  return {
    type: Keys.GET_USER_INFO,
  };
};

export const getUserInfoSuccess = (
  res: IUserInfo,
): IActions.IGetUserInfoSuccess => {
  return {
    type: Keys.GET_USER_INFO_SUCCESS,
    payload: res,
  };
};

export const getUserInfoFail = (res: any): IActions.IGetUserInfoFail => {
  return {
    type: Keys.GET_USER_INFO_FAIL,
    payload: {
      errors: res,
    },
  };
};
//#endregion

//#region Update User Info Actions
export const updateUserInfo = (data: any): IActions.IUpdateUserInfo => {
  return {
    type: Keys.UPDATE_USER_INFO,
    payload: data,
  };
};

export const updateUserInfoSuccess = (
  res: any,
): IActions.IUpdateUserInfoSuccess => {
  return {
    type: Keys.UPDATE_USER_INFO_SUCCESS,
    payload: res,
  };
};

export const updateUserInfoFail = (res: any): IActions.IUpdateUserInfoFail => {
  return {
    type: Keys.UPDATE_USER_INFO_FAIL,
    payload: {
      errors: res,
    },
  };
};
//#endregion

//#region Upload File Actions
export const uploadFiles = (data: {file: ImagePickerResult}): IActions.IUploadFiles => {
  return {
    type: Keys.UPDATE_FILES,
    payload: data,
  };
};

export const uploadFilesSuccess = (res: {
  url: string;
}): IActions.IUploadFilesSuccess => {
  return {
    type: Keys.UPDATE_FILES_SUCCESS,
    payload: res,
  };
};

export const uploadFilesFail = (res: any): IActions.IUploadFilesFail => {
  return {
    type: Keys.UPDATE_FILES_FAIL,
    payload: {
      errors: res,
    },
  };
};
//#endregion
