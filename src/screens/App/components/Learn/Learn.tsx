import React from 'react';
import {StyleSheet, TouchableWithoutFeedback, Alert} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Feather} from '@expo/vector-icons';
import {Audio} from 'expo-av';
import {Box, Text, ProcessBar, Button, theme} from '../../../../components';
import {AppRoutes, StackNavigationProps} from '../../../../navigation';
import {IAnswer, IUserAnswer} from '../../../../redux/model/IApp';
import {
  Footer,
  QuizQuestion,
  ListeningQuestion,
  SentenceQuestion,
} from './components';

import {AppProps} from './LearnContainer';

type LearnNavigation = StackNavigationProps<AppRoutes, 'Learn'>;
export interface LearnProps extends LearnNavigation, AppProps {}

const Learn: React.FC<LearnProps> = ({navigation, store, actions}) => {
  const [questionIndex, setQuestionIndex] = React.useState<number>(0);
  const {currentLesson, isProcessing, isHistoryCreatedSuccessful} = store.App;
  const questionLength = currentLesson.questions.length;
  const [selectedAnswer, setSelectedAnswer] = React.useState<IAnswer>(
    undefined,
  );
  const [finalResult, setFinalResult] = React.useState<'fail' | 'passed' | ''>(
    '',
  );
  const [result, setResult] = React.useState<IUserAnswer>(undefined);
  // const [sound, setSound] = React.useState(undefined);

  React.useEffect(() => {
    if (isHistoryCreatedSuccessful) {
      actions.handleClear({
        type: 'postUserHistory',
      });
      navigation.navigate('Home');
    }
  }, [isHistoryCreatedSuccessful]);

  async function playSound(right: boolean) {
    const {sound} = await Audio.Sound.createAsync(
      right
        ? require('../../../../../assets/sound/right-answer.mp3')
        : require('../../../../../assets/sound/wrong-answer.mp3'),
    );

    await sound.playAsync();
  }

  const onCalcProcessWidth = () => {
    return (questionIndex / questionLength) * 100;
  };

  const onQuitLesson = () =>
    Alert.alert(
      'Thông báo',
      'Bạn có chắc chắn tiếp tục, kết quả bài học sẽ bị hủy!',
      [
        {
          text: 'Hủy',
        },
        {text: 'Tiếp tục', onPress: () => navigation.navigate('Home')},
      ],
    );

  const onNextQuestion = () => {
    if (questionIndex + 1 < questionLength) {
      setQuestionIndex(questionIndex + 1);
      setResult(undefined);
      setSelectedAnswer(undefined);
    } else {
      setFinalResult('passed');
    }
  };

  const onCheckAnswer = () => {
    const currentQuestion = currentLesson.questions[questionIndex];
    const result: IUserAnswer = {
      questionId: currentQuestion._id,
      userAnswer: selectedAnswer.answer,
      rightAnswer: currentQuestion.rightAnswer,
      question: currentQuestion.question,
    };
    playSound(selectedAnswer.answer === currentQuestion.rightAnswer);
    actions.handleUserAnswer(result);
    setResult(result);
  };

  const renderQuestionWithType = (index: number) => {
    const currentQuestion = currentLesson.questions[index];

    switch (currentQuestion.type) {
      case 'sentence':
        return (
          <SentenceQuestion
            onSelectAnswer={(data: IAnswer) => setSelectedAnswer(data)}
            selectedAnswer={selectedAnswer}
            question={currentQuestion}
          />
        );
      case 'listening':
        return (
          <ListeningQuestion
            onSelectAnswer={(data: IAnswer) => setSelectedAnswer(data)}
            selectedAnswer={selectedAnswer}
            question={currentQuestion}
          />
        );

      default:
        return (
          <QuizQuestion
            onSelectAnswer={(data: IAnswer) => setSelectedAnswer(data)}
            selectedAnswer={selectedAnswer}
            question={currentQuestion}
          />
        );
    }
  };

  if (finalResult === 'passed') {
    return (
      <SafeAreaView style={styles.container}>
        <Box flex={1} paddingHorizontal="m">
          <Box flex={2} justifyContent="center" alignContent="center">
            <Text variant="header" textAlign="center">
              Bạn đã hoàn thành bài học
            </Text>
          </Box>
          <Box flex={1} justifyContent="center">
            <Button
              variant="primary"
              disabled={isHistoryCreatedSuccessful || isProcessing}
              label={isHistoryCreatedSuccessful ? 'Đang xử lý' : 'Tiếp tục'}
              onPress={() => {
                actions.postUserHistory({
                  isAddGamesLevel: true,
                  isAddVocabularyLevel: false,
                  gameId: currentLesson._id,
                });
              }}
            />
          </Box>
        </Box>
      </SafeAreaView>
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      <Box
        flexDirection="row"
        padding="m"
        alignItems="center"
        justifyContent="space-between">
        <Box marginRight="s">
          <TouchableWithoutFeedback onPress={onQuitLesson}>
            <Feather name="x" size={40} color={theme.colors.alto} />
          </TouchableWithoutFeedback>
        </Box>
        <ProcessBar width={`${onCalcProcessWidth()}%`} />
      </Box>

      {currentLesson !== undefined &&
        questionIndex < questionLength &&
        renderQuestionWithType(questionIndex)}
      <Footer
        onPressContinue={() => onNextQuestion()}
        onCheckAnswer={() => onCheckAnswer()}
        selectedAnswer={selectedAnswer}
        result={result}
      />
    </SafeAreaView>
  );
};

export default Learn;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
});
