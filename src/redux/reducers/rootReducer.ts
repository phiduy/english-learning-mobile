/**
 * @file Root reducers
 */

import {combineReducers} from 'redux';
import IStore from '../model/store/IStore';

// Place for reducers' app
import AppState, {name as nameOfAppState} from '../reducers/appReducer';
import AuthenticationState, {
	name as nameOfAuthenticationState,
} from '../reducers/authenticationReducer';

/*----Reducers List-----------------*/
const reducers = {
	[nameOfAppState]: AppState,
	[nameOfAuthenticationState]: AuthenticationState,
};

export default combineReducers<IStore>(reducers);