import React from 'react';
import {
  StyleSheet,
  ScrollView,
  Dimensions,
  NativeScrollEvent,
  NativeSyntheticEvent,
  Image,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Box, HeaderBar, Text, theme} from '../../../../components';
import {AppTabs} from '../../../../navigation';
import {BottomTabNavigationProp} from '@react-navigation/bottom-tabs';
import {AppProps} from './VocabularyContainer';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {VocabularyLearn} from './components/VocabularyLearn';
import {IVocabularyPackage} from '../../../../redux/model/IApp';

export type VocabularyBottomNavigationProps = BottomTabNavigationProp<
  AppTabs,
  'Vocabulary'
>;
export interface VocabularyProps
  extends VocabularyBottomNavigationProps,
    AppProps {}

const {height} = Dimensions.get('window');

const Vocabulary: React.FC<VocabularyProps> = (props) => {
  const {actions, store} = props;
  const {vocabularyPackages, isLoadingVocabularyPackage} = store.App;
  const [contentOffsetY, setContentOffsetY] = React.useState(0);
  const [toggleChildScreen, setChildScreen] = React.useState<
    'lesson' | 'learn' | 'packages'
  >('packages');
  const [currentLesson, setCurrentLesson] = React.useState<IVocabularyPackage>(
    undefined,
  );

  React.useEffect(() => {
    actions.getVocabularyPackages({root: true});
    return () => {
      setChildScreen('lesson');
    };
  }, []);

  const onScroll = (e: NativeSyntheticEvent<NativeScrollEvent>) => {
    const contentOffset = e.nativeEvent.contentOffset.y;
    contentOffsetY < contentOffset
      ? console.log('Scroll Down')
      : console.log('Scroll Up');
    setContentOffsetY(contentOffset);
  };

  if (toggleChildScreen === 'learn') {
    return (
      <VocabularyLearn
        currentLesson={currentLesson}
        setChildScreen={() => setChildScreen('lesson')}
      />
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      <HeaderBar
        label="Từ vựng"
        showBackButton={toggleChildScreen !== 'packages'}
        onBackPress={() => {
          actions.getVocabularyPackages({
            root: true,
          });
          setChildScreen('packages');
        }}
      />
      {isLoadingVocabularyPackage ? (
        <Box flex={1} justifyContent="center" alignContent="center">
          <Text variant="title" textAlign="center">
            Loading...
          </Text>
        </Box>
      ) : (
        <ScrollView
          style={{flex: 1, backgroundColor: theme.colors.white}}
          onScroll={onScroll}>
          <Box flex={1} paddingHorizontal="m" marginBottom="m">
            {vocabularyPackages &&
              vocabularyPackages.data.map((item) => (
                <TouchableWithoutFeedback
                  key={item._id}
                  onPress={() => {
                    if (toggleChildScreen === 'lesson') {
                      setCurrentLesson(item);
                      setChildScreen('learn');
                      console.log('item', item);
                    } else {
                      setChildScreen('lesson');
                      actions.getVocabularyPackages({
                        parent: item._id,
                      });
                    }
                  }}>
                  <Box
                    padding="m"
                    justifyContent="flex-start"
                    alignItems="center"
                    flexDirection="row"
                    borderBottomColor="borderColorDefault"
                    borderBottomWidth={1}>
                    <Box marginRight="m">
                      <Image
                        resizeMode="center"
                        style={{
                          width: 50,
                          height: 50,
                        }}
                        source={{
                          uri: item.packageImage,
                        }}
                      />
                    </Box>
                    <Text variant="title">{item.name}</Text>
                  </Box>
                </TouchableWithoutFeedback>
              ))}
          </Box>
        </ScrollView>
      )}
    </SafeAreaView>
  );
};

export default Vocabulary;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
});
