import Keys from './actionKeys';
import * as IActions from './IActions';

export const handleClear = (data: {type: 'all'}): IActions.IHandleClear => {
  return {
    type: Keys.HANDLE_CLEAR,
    payload: data,
  };
};

export const getStoredAuthentication = (data: {
  accessToken: string;
}): IActions.IGetStoredAuthentication => {
  return {
    type: Keys.GET_STORED_AUTHENTICATION,
    payload: data,
  };
};

//#region User Login Actions
export const userLogin = (data: {
  email: string;
  password: string;
}): IActions.IUserLogin => {
  return {
    type: Keys.USER_LOGIN,
    payload: data,
  };
};

export const userLoginSuccess = (res: any): IActions.IUserLoginSuccess => {
  return {
    type: Keys.USER_LOGIN_SUCCESS,
    payload: res,
  };
};

export const userLoginFail = (res: any): IActions.IUserLoginFail => {
  return {
    type: Keys.USER_LOGIN_FAIL,
    payload: res,
  };
};
//#endregion

//#region User Register Actions
export const userRegister = (data: {
  email: string;
  password: string;
}): IActions.IUserRegister => {
  return {
    type: Keys.USER_REGISTER,
    payload: data,
  };
};

export const userRegisterSuccess = (
  res: any,
): IActions.IUserRegisterSuccess => {
  return {
    type: Keys.USER_REGISTER_SUCCESS,
    payload: res,
  };
};

export const userRegisterFail = (res: any): IActions.IUserRegisterFail => {
  return {
    type: Keys.USER_REGISTER_FAIL,
    payload: res,
  };
};
//#endregion
