import React from 'react';
import {StyleSheet} from 'react-native';
import {Box, Text} from '../../../../../../components';
import {IAnswer, IQuestion} from '../../../../../../redux/model/IApp';
import {AnswerOption} from '../Option';
interface ISentenceQuestionProps {
  question: IQuestion;
  selectedAnswer: IAnswer;
  onSelectAnswer: (data: IAnswer) => void;
}

const wordReducer = (
  state: IAnswer[],
  action: {type: 'add' | 'remove'; word: IAnswer},
) => {
  switch (action.type) {
    case 'add':
      return [...state, action.word];
    case 'remove':
      const update = [...state];
      const wordIndex = state.findIndex((item) => item._id === action.word._id);
      if (wordIndex < 0) {
        return state;
      }
      update.splice(wordIndex, 1);
      return update;
    default:
      return state;
  }
};

const SentenceQuestion: React.FC<ISentenceQuestionProps> = ({
  question,
  selectedAnswer,
  onSelectAnswer,
}) => {
  const [selectedWord, setSelectedWord] = React.useReducer(wordReducer, []);
  const {answers} = question;

  React.useEffect(() => {
    onSelectAnswer({
      answer: selectedWord.map((word) => word.answer).join(' '),
      _id: question._id,
      audio: '',
      image: '',
    });
    // eslint-disable-next-line
  }, [selectedWord]);

  const onSelectWord = (word: IAnswer) => {
    const index = selectedWord.indexOf(word);
    setSelectedWord({word, type: index < 0 ? 'add' : 'remove'});
  };

  const renderSelectedWords = () => {
    return (
      <Box
        flexDirection="row"
        height={60}
        borderBottomWidth={2}
        borderBottomColor="borderColorDefault"
        justifyContent="flex-start">
        {selectedWord.map((word) => (
          <Box marginRight="s" key={word._id}>
            <Text variant="title2">{word.answer}</Text>
          </Box>
        ))}
      </Box>
    );
  };

  return (
    <React.Fragment>
      <Box paddingHorizontal="m" marginBottom="m">
        <Text variant="title">{question.question}</Text>
      </Box>
      <Box paddingHorizontal="m" marginBottom="m">
        {renderSelectedWords()}
      </Box>
      <Box
        flex={3}
        flexDirection="row"
        flexWrap="wrap"
        paddingHorizontal="m"
        justifyContent="space-around"
        alignItems="flex-end">
        {answers.map((item) => (
          <AnswerOption
            onSelectAnswer={() => {
              onSelectWord(item);
              onSelectAnswer(item);
            }}
            selected={selectedWord.indexOf(item) > -1}
            key={item._id}
            item={item}
            inline={true}
            onlyLabel={true}
          />
        ))}
      </Box>
    </React.Fragment>
  );
};
const styles = StyleSheet.create({});
export default SentenceQuestion;
