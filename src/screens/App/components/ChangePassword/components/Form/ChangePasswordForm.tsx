import React from 'react';
import {Formik, FormikHelpers} from 'formik';
import {Alert} from 'react-native';
import * as yup from 'yup';
import {Box, Text, TextInput, Button} from '../../../../../../components';
import {useDispatch} from 'react-redux';
import {updateUserInfo} from '../../../../../../redux/actions/app';

const validationProfileSchema = yup.object().shape({
  oldPassword: yup.string().required('Mật khẩu cũ là trường bắt buộc'),
  password: yup.string().required('Mật khẩu mới là trường bắt buộc'),
  rePassword: yup.string().required('Nhập lại mật khẩu mới là trường bắt buộc'),
});

interface FormUpdateInfoValues {
  oldPassword: string;
  password: string;
  rePassword: string;
}

const ChangePasswordForm = () => {
  const dispatch = useDispatch();

  const initialValues = {
    rePassword: '',
    oldPassword: '',
    password: '',
  };

  const onSubmit = (
    values: FormUpdateInfoValues,
    {setSubmitting}: FormikHelpers<FormUpdateInfoValues>,
  ) => {
    dispatch(
      updateUserInfo({
        oldPassword: values.oldPassword,
        password: values.password,
      }),
    );
  };

  const showError = (errors: string, setErrors: () => void) => {
    Alert.alert('Thông báo', errors, [
      {
        text: 'Ok',
        onPress: setErrors,
      },
    ]);
  };

  return (
    <React.Fragment>
      <Formik
        validateOnBlur={false}
        validateOnChange={true}
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationProfileSchema}>
        {({handleChange, handleSubmit, errors, values, isValid}) => (
          <>
            <Box marginBottom="l">
              <TextInput
                label="Mật khẩu cũ"
                value={values.oldPassword}
                placeholder="Mật khẩu cũ"
                onChangeText={handleChange('oldPassword')}
                secureTextEntry={true}
              />
              <TextInput
                label="Mật khẩu mới"
                value={values.password}
                placeholder="Mật khẩu mới"
                onChangeText={handleChange('password')}
                secureTextEntry={true}
              />
              <TextInput
                label="Xác nhận mật khẩu"
                value={values.rePassword}
                placeholder="Password"
                onChangeText={handleChange('rePassword')}
                secureTextEntry={true}
              />
              {!isValid && (
                <Box marginBottom="m">
                  <Text
                    variant="body"
                    color="persianRed"
                    fontSize={15}
                    letterSpacing={2}>
                    {errors[Object.keys(errors)[0]]}
                  </Text>
                </Box>
              )}

              <Button
                variant="secondary"
                label="Thay đổi"
                onPress={handleSubmit}
                disabled={!isValid}
              />
            </Box>
          </>
        )}
      </Formik>
    </React.Fragment>
  );
};

export default ChangePasswordForm;
