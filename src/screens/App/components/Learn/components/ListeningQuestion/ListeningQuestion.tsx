import React from 'react';
import {StyleSheet} from 'react-native';
import {Box, Text, theme} from '../../../../../../components';
import {IAnswer, IQuestion} from '../../../../../../redux/model/IApp';
import {AnswerOption} from '../Option';
import {Audio} from 'expo-av';
import {BASE_URL} from '../../../../../../common/constants';
import {AntDesign} from '@expo/vector-icons';
interface IListeningProps {
  question: IQuestion;
  selectedAnswer: IAnswer;
  onSelectAnswer: (data: IAnswer) => void;
}

const Listening: React.FC<IListeningProps> = ({
  question,
  selectedAnswer,
  onSelectAnswer,
}) => {
  const {answers} = question;
  const sound = new Audio.Sound();

  React.useEffect(() => {
    // playSound(`${BASE_URL}${question.link}`);
    return sound
      ? () => {
          console.log('Unloading Sound');
          sound.unloadAsync();
        }
      : undefined;
  }, [sound]);

  async function playSound(url: string) {
    await sound.loadAsync({
      uri: url,
    });
    sound.playAsync();
  }

  return (
    <React.Fragment>
      <Box paddingHorizontal="m" marginBottom="l">
        <Text variant="title">{question.question}</Text>
      </Box>
      <Box
        paddingHorizontal="m"
        marginBottom="l"
        flexDirection="row"
        justifyContent="center">
        <Box
          style={styles.audio}
          onTouchStart={() => playSound(`${BASE_URL}${question.link}`)}>
          <AntDesign name="sound" size={24} color="white" />
        </Box>
      </Box>
      <Box
        flex={3}
        flexDirection="row"
        flexWrap="wrap"
        paddingHorizontal="m"
        justifyContent="space-around"
        alignItems="flex-end">
        {answers.map((item) => (
          <AnswerOption
            onSelectAnswer={() => onSelectAnswer(item)}
            selected={item._id === selectedAnswer?._id}
            key={item._id}
            item={item}
            onlyLabel={true}
          />
        ))}
      </Box>
    </React.Fragment>
  );
};
const styles = StyleSheet.create({
  audio: {
    width: 120,
    height: 80,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 16,
    borderWidth: 2,
    borderColor: theme.colors.borderColorSecondary,
    borderBottomWidth: 4,
    backgroundColor: theme.colors.secondary,
  },
});

export default Listening;
