import AsyncStorage from '@react-native-async-storage/async-storage';

export const getItem = async (key: string) => {
	try {
		const result = await AsyncStorage.getItem(key);
		return result as string;
	} catch (e) {
		return null;
	}
};

export const setItem = async (key, value) => {
	try {
		await AsyncStorage.setItem(key, value);
	} catch (e) {}
	console.log('Set Done');
};

export const removeFewItems = async (keys: string[]) => {
	try {
		await AsyncStorage.multiRemove(keys);
	} catch (e) {}
	console.log('Remove Done');
};
