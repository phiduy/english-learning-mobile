import React from 'react';
import {StyleSheet} from 'react-native';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Box, Text, Button, theme, HeaderBar} from '../../../components';
import {Feather} from '@expo/vector-icons';
import {AuthenticationRoutes, StackNavigationProps} from '../../../navigation';
import {ForgotPasswordForm} from './components';

type ForgotPasswordNavigation = StackNavigationProps<
  AuthenticationRoutes,
  'ForgotPassword'
>;
export interface ForgotPasswordProps extends ForgotPasswordNavigation {}
const ForgotPassword: React.FC<ForgotPasswordProps> = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <Box flex={1} padding="m" position="relative">
        <HeaderBar
          border={false}
          onBackPress={() => navigation.navigate('Login')}
        />
        <Box
          marginBottom="xl"
          alignItems="center"
          flex={1}
          justifyContent="flex-end">
          <Text variant="title" marginBottom="m">
            Quên mật khẩu?
          </Text>
          <Text variant="subTitle" textAlign="center">
            Nhập địa chỉ email để nhận đường dẫn đặt lại mật khẩu
          </Text>
        </Box>

        <ForgotPasswordForm />
      </Box>
    </SafeAreaView>
  );
};

export default ForgotPassword;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});
