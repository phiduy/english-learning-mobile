import {IAppState, name as AppState} from '../IApp';
import {
	IAuthenticationState,
	name as AuthenticationState,
} from '../IAuthentication';

export default interface IStore {
	[AppState]: IAppState;
	[AuthenticationState]: IAuthenticationState;
}
export interface StoreProps<T> {
	store: IStore;
	actions: T;
}
