import {call, put, fork, all, takeEvery} from 'redux-saga/effects';
import {Keys} from '../redux/actions/authentication';
import * as actions from '../redux/actions/authentication/actions';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as authApi from '../api/authentication';

export function* saveTokenToStore(data) {
  try {
    yield AsyncStorage.setItem('authentication', JSON.stringify(data));
    console.log('save success');
  } catch (err) {
    console.log('save fail');
  }
}

export function* handleUserLogin(action: any) {
  try {
    const res = yield call(authApi.userLogin, action.payload);
    if (res.status === 200) {
      yield call(saveTokenToStore, res.data.data);
      yield put(actions.userLoginSuccess(res.data.data));
    } else {
      yield put(actions.userLoginFail(res.data.errors));
    }
  } catch (err) {
    yield put(actions.userLoginFail(err));
  }
}

export function* handleUserRegister(action: any) {
  try {
    const res = yield call(authApi.userLogin, action.payload);
    if (res.status === 200) {
      yield put(actions.userRegisterSuccess(res.data.data));
    } else {
      yield put(actions.userRegisterFail(res.data.errors));
    }
  } catch (err) {
    yield put(actions.userRegisterFail(err));
  }
}

/*-----------------------------------------------------------------*/
export function* watchUserLogin() {
  yield takeEvery(Keys.USER_LOGIN, handleUserLogin);
}
export function* watchUserRegister() {
  yield takeEvery(Keys.USER_REGISTER, handleUserRegister);
}
/*-----------------------------------------------------------------*/
export const main = [watchUserLogin, watchUserRegister];

export default function* appSaga() {
  yield all([...main.map((saga) => fork(saga))]);
}
