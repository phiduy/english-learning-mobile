import React from 'react';
import {Formik, FormikHelpers} from 'formik';
import {Alert} from 'react-native';
import * as yup from 'yup';
import {Box, Text, TextInput, Button, theme} from '../../../../../components';
import {Feather} from '@expo/vector-icons';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {useDispatch, useSelector} from 'react-redux';
import {userLogin} from '../../../../../redux/actions/authentication';
import IStore from '../../../../../redux/model/store/IStore';

const validationLoginSchema = yup.object().shape({
  email: yup
    .string()
    .email('Email không hợp lệ')
    .required('Email là trường bắt buộc'),
  password: yup.string().required('Mật khẩu là trường bắt buộc'),
});

interface FormLoginValues {
  email: string;
  password: string;
}

const LoginForm = () => {
  const [visiblePassword, setVisiblePassword] = React.useState(false);
  const dispatch = useDispatch();
  const AuthState = useSelector((state: IStore) => state.Auth);
  const {isProcessing} = AuthState;

  const initialValues = {
    email: '',
    password: '',
  };

  const onSubmit = (
    values: FormLoginValues,
    {setSubmitting}: FormikHelpers<FormLoginValues>,
  ) => {
    dispatch(userLogin(values));
  };

  const showError = (errors: string, setErrors: () => void) => {
    Alert.alert('Thông báo', errors, [
      {
        text: 'Ok',
        onPress: setErrors,
      },
    ]);
  };

  return (
    <React.Fragment>
      <Formik
        validateOnBlur={false}
        validateOnChange={true}
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationLoginSchema}>
        {({
          handleChange,
          setFieldValue,
          setErrors,
          handleSubmit,
          errors,
          values,
          isValid,
        }) => (
          <>
            <Box marginBottom="l">
              <TextInput
                value={values.email}
                placeholder="Email"
                onChangeText={handleChange('email')}
              />
              <TextInput
                value={values.password}
                placeholder="Mật khẩu"
                onChangeText={handleChange('password')}
                secureTextEntry={visiblePassword ? false : true}
                suffix={
                  <TouchableWithoutFeedback
                    onPress={() => setVisiblePassword(!visiblePassword)}>
                    <Feather
                      name={visiblePassword ? 'eye-off' : 'eye'}
                      size={24}
                      color={theme.colors.muted}
                    />
                  </TouchableWithoutFeedback>
                }
              />
              {!isValid && (
                <Box marginBottom="m">
                  <Text
                    variant="body"
                    color="persianRed"
                    fontSize={15}
                    letterSpacing={2}>
                    {errors[Object.keys(errors)[0]]}
                  </Text>
                </Box>
              )}

              <Button
                loading={isProcessing}
                variant="secondary"
                label="Đăng nhập"
                onPress={handleSubmit}
                disabled={!isValid || !values.email || !values.password}
              />
            </Box>
          </>
        )}
      </Formik>
    </React.Fragment>
  );
};

export default LoginForm;
