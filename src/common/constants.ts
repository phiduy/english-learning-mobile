export interface IError {
  code: string;
  error: string;
}

export const BASE_URL = 'http://staging.pd-mobile-web.tk/';
export const API_URL = 'http://staging.pd-mobile-web.tk/api';

export const SOUND_EFFECT = {
  wrongAnswer: require('../../assets/sound/wrong-answer.mp3'),
  rightAnswer: require('../../assets/sound/right-answer.mp3'),
  steak: require('../../assets/sound/steak.mp3'),
};

export const ErrorCodeMessages: IError[] = [
  {
    code: 'usersUsersNotFound',
    error: 'Không tìm thấy người dùng !',
  },
  {
    code: 'usersWrongPassword',
    error: 'Sai mật khẩu !',
  },
  {
    code: 'serverError',
    error: 'Server xảy ra lỗi !',
  },
];
