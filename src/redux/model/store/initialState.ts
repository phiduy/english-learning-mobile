import IStore from './IStore';
import {initialState as AppInitialState, name as AppState} from '../IApp';
import {
	initialState as AuthenticationInitialState,
	name as AuthenticationState,
} from '../IAuthentication';

const initialState: IStore = {
	[AppState]: AppInitialState,
	[AuthenticationState]: AuthenticationInitialState,
};

export default initialState;
